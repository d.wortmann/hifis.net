---
title: "Experience Report: Machine Learning Hackathon at GFZ"
date: 2020-04-20
authors:
  - dolling
layout: blogpost
title_image: default
categories:
  - report
tags:
  - hackathon
excerpt:
    "
    In March the first machine learning hackathon was held at GFZ Potsdam. 
    Here I give an overview from the organizers point of view.
    "
---

The Machine Learning (ML) Group of the GFZ was lucky that one of the last events that happened in person was our **machine learning hackathon**.
It was held on March 5th, 2020 and organized by the ML Interest Group that formed at GFZ in 2019.
The group mainly consists of researchers from different domain of Earth sciences, having common interests in ML state of the art techniques and tools.
The hackathon idea came through a survey we conducted among its members. 

Okay, now you might be wondering how we set it up.
Before I give you the details, I want you to keep in mind that it was our first time hosting a hackathon.
We had a list of wishes: 
	1. We wanted it to be family friendly and flexible
	2. Our members cover experience ranges from beginner to advanced, so we wanted to be inclusive of everyone
	3. We also wanted the hackathon to be a one day event.

Once we had clarified our wish list, we needed to focus on how to make it happen. 
Our goal was to find a set of interesting ML problems that benefit our participants and are still feasible to be addressed within the hackathon's time-frame.
So we approached participants to submit problems themselves. 
Keeping in mind our participants' mixed experience levels from novices to experts, we then selected submitted challenges that were suitable to all of them.

The first hackathon challenge was a shearing problem in tectonics given a time series dataset for seismic activities.
The participants where free to explore the data as they wished.
The second problem was about finding out if spectral satellite data can be used to understand soil composition.

On the day of the hackaton, thirty participants joined us.
The chosen problems were initially presented by participants who proposed them, then the remaining participants were given the chance to choose the problem that interested them. 
Afterwards, we organized three to six people each into a group to hack a solution of their choice.
As the hackaton was intended to discuss, share ideas and come up with a solution, it was not a competitive environment.
We experienced a friendly atmosphere where people freely discuss and even had pizzas and drinks.

At the end of the day, all groups presented their results and also uploaded it to our [GitLab group][MLGFZ]. 
It was great to see different approaches for a solution from different groups.
One of the interesting outcomes was that people from different research domains found out that they are using similar methodology which opened up ways for future collaborations.
This shows that such events can have a long term impact.

I was happy to be part of such interesting and collaborative environment!

Of course, we always aim to improve such events.
For this purpose we are looking at participant's feedback to take into account for our next event.

[MLGFZ]: https://git.gfz-potsdam.de/ml-gfz
