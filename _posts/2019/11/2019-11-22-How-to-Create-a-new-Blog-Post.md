---
title: How to Create a New Blog Post?
date: 2019-11-22
authors:
  - hueser
layout: blogpost
title_image: default
categories: [tutorial]
tags: [startblogging, guidedtour]
additional_css:
  - 2019-11-22-Workflow-to-Create-a-new-Blog-Post/create_blog_post.css
excerpt: 
  Nice project web page, how can I contribute with my own blog post?
  This is what this blog post is about.
  I will explain the workflow of &ldquo;how to create a new blog post&rdquo; and
  thereby illustrate how to use the tools Git, GitLab and Jekyll
  for these purposes.
---

## TL;DR

{:.summary}
_Nice project web page, how can I contribute with my own blog post?_
This is what this blog post is about.
I will explain the workflow of "how to create a new blog post" and 
thereby illustrate how to use the tools _Git_, _GitLab_ and _Jekyll_
for these purposes.

There are minimal requirements for a blogger to add a new blog post
while a proper review process is still essential for the quality of
the blog posts.
So let us jump into it.

Note:
In case you are interested to know how to contribute 
to _HIFIS Software_ web-page in general besides writing blog posts
you can find more information in the Contribution Guide
<sup>[[1]](https://gitlab.hzdr.de/hifis/software.hifis.net/blob/master/CONTRIBUTING.md "Contribution Guide")</sup>.

## Tools explained

First, I would like to introduce three tools which are very helpful
if you are going to write a blog post.

**Version Control System _Git_**

_Git_
<sup>[[2]](https://en.wikipedia.org/wiki/Git "Git (Wikipedia)")</sup>
is a decentralized version control system which lets you
store all files of your project and all snapshots / versions
of your changes.
It lets you track back the complete history
with your log messages given.
Decentralized means the whole repository resides as a copy on your
local computer.

**Version Management Platform _GitLab_**

_GitLab_
<sup>[[3]](https://en.wikipedia.org/wiki/GitLab "GitLab (Wikipedia)")</sup>
provides you with user interfaces for several actions which can be
done on the remote server without downloading the whole repository
onto your local computer.
There is also an issue tracker included.
On top, there is a full _Web IDE_ available which also let you review
changes by your colleagues.
Even if you are new to _Git_, the _GitLab Web IDE_ offers an easy 
interface which can be employed for most of your Use Cases.
There is no need to install _Git_, download the _Git_ repository to
your local machine and set up your own IDE
to contribute with your blog posts.

**Static Web Site Generator _Jekyll_**

_Jekyll_
<sup>[[4]](https://en.wikipedia.org/wiki/Jekyll_(software) "Jekyll (Wikipedia)")</sup>
is a static web site generator.
You only write _Markdown_ text files and _Jekyll_ takes care of the
rest and generates a static web site containing only HTML and CSS
according to your templates and layouts given.

## Workflow in a Nutshell

Now that we know more about the tools used, I would like to introduce
the steps I took to create this blog post.
You can stick to these steps if you are writing your own blog post:

1. Share, discuss or pick a post idea
2. Write and share your post
3. The review process
4. Your post is online

By agreeing to this process we put you into the position of a
fully authorized and equitable team member who is able to actually
easily contribute with your own work to this web page.

On closer inspection, there are the following persuasive arguments
to use _Git_, _GitLab_ and _Jekyll_:

* You can collaborate with different authors and reviewers using a
decentralized version control system like _Git_.
* There is a full traceback and logging functionality included in _Git_.
* _GitLab_ lets you review, discuss and improve what you write in
a well thought-out review process called _Merge Request_.
* _GitLab Web IDE_ is actually a surrogate for a local IDE with
_Git_ integration.
* _GitLab CI Pipelines_ make sure and check that all is still
working as expected.
* With _Jekyll_ you just need to know very few syntactic markdown
features, there is no need to write your own HTML and CSS.

Finally, to make a clear statement ...
**_you don't end up in a mess with multiple versions of
a document floating around on local computers of
several contributors._**

## Going into Details - a Step-by-step Guided Tour

**1. Share, Discuss or Pick a Post Idea**

First of all, you need some sound ideas regarding the topic you would
like to blog about.

Open a new issue in the _GitLab UI_.
In order to do so, you need an account in GitLab of HZDR
<sup>[[5]](https://gitlab.hzdr.de "HZDR GitLab Instance")</sup>
and being added as member to the respective GitLab project.
Opening an issue is explained in the _GitLab Docs_
<sup>[[6](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html "Managing GitLab Issues")</sup>.
It is essential to give it a comprehensive _title_ and _description_
which reflects the basic idea of the topic.
It is recommended to use the predefined _Issue Template_ "_blog-post_"
which guides you through the important questions that need to be 
answered in order to start a fruitful discussion.
By doing so you let us know what you would like to write about.
Assign this issue to the main blog author (which usually is 
yourself).
If you use the template this will be done automatically for you.
The images below show such a blog-post issue as well as the 
pre-defined description text and questions to answer.

![Blog Post Template View]({{ site.directory.images | append: 'posts/2019-11-22-Workflow-to-Create-a-new-Blog-Post/Screenshot_Blog_Post_Template_View_20191113.png' | relative_url }} "Blog Post Template View"){:.blog_post_template_image}

![Blog Post Template Text]({{ site.directory.images | append: 'posts/2019-11-22-Workflow-to-Create-a-new-Blog-Post/Screenshot_Blog_Post_Template_Text_20191113.png' | relative_url }} "Blog Post Template Text"){:.blog_post_template_image}

**2. Write and Share your Post**

The next step is to create a Feature Branch based on branch _master_
and give it a meaningful name like `<issue_number>-<branch_name>`.
This can be done within the _GitLab User-Interfaces_
<sup>[[7]](https://docs.gitlab.com/ce/user/project/repository/web_editor.html#create-a-new-branch "Create a new Branch in GitLab")</sup>.

Navigate into this branch in _GitLab_ and open _GitLab Web IDE_
<sup>[[8]](https://docs.gitlab.com/ce/user/project/web_ide/#open-the-web-ide "Open GitLab Web IDE")</sup>.
_GitLab Web IDE_ is the preferred tool to contribute if you are
unfamiliar with _Git_.

Notice the file system tree on the left hand side of the _Web IDE UI_.
You can find a template for your blog post in folder `_posts/<year>/<month>`.
The file name is _2019-10-29-Blog-Post-Template.md_.
Feel free to copy it, give it a name according to the pattern
`YYYY-MM-DD-My-post-title.md`, put your new file into folder `_posts/<year>/<month>`,
and use it for your own purposes.
The template explains the so called _Frontmatter_ which contains
variables like `title`, `date`, `authors`, etc.
Additionally, the most important markdown is explained by example
within the template as well.
Of course you want to remove the explanations and examples except for
the _Frontmatter_ at the top of your markdown file.

Another helpful feature of the _GitLab Web IDE_ is the
_Preview Markdown_ pane.
You can check that everything you wrote is displayed as intended.

Now you are ready to start writing your _Markdown_ file following the specific
syntax of _Markdown_ files
<sup>[[9]](https://docs.gitlab.com/ce/user/markdown.html "GitLab Markdown")</sup>.

Once you are done with your editing you would like to _stage_ and _commit_
your changes 
<sup>[[10]](https://docs.gitlab.com/ce/user/project/web_ide/#stage-and-commit-changes "Stage and Commit Changes in GitLab Web IDE")</sup> 
giving it a meaningful message explaining your changes.
There is a blog post
<sup>[[11]](https://chris.beams.io/posts/git-commit/ "Best Practice of Git Commit Messages")</sup>
about best practices for writing _Git_ commit messages.
The more verbose your messages are, the easier it is for others
to understand your changes.

**3. The Review Process**

After some effort and a few commits, let us assume your blog post 
is in a state where you consider it ready to see the light of day.
What you want to do at this point is to open a _Merge Request_
<sup>[[12]](https://docs.gitlab.com/ce/user/project/merge_requests/ "Merge Request")</sup>.
_GitLab's User-Interface_ lets you open _Merge Requests_
<sup>[[13]](https://docs.gitlab.com/ce/user/project/merge_requests/creating_merge_requests.html "Creating a Merge Request in GitLab")</sup>.
Give it a meaningful title and description and mention the
original issue as well (e.g. by stating `Closes #<issue_number>.` in
the description).

Assign your _Merge Request_ to one or multiple persons you would
like to review the blog post.
This is an important quality gate.
Do not rely on reviewing your own work.
Please select at least one other reviewer.

In order to make the review process as easy as possible GitLab
offers a feature called _Review Apps_ to inspect your branch.
This enables the blog post authors as well as reviewers to preview
the page in a test environment.
As soon as your your branch is pushed onto the remote
repository and the _GitLab CI Pipeline_ responsible for
deploying your branch finished successfully,
you can inspect your latest deployments of your branch
by clicking the button _View app_ in the User-Interface of your
Merge Request.

![Review Apps Button]({{ site.directory.images | append: 'posts/2019-11-22-Workflow-to-Create-a-new-Blog-Post/Screenshot_Review_Apps_01_20191121.png' | relative_url }} "Review Apps Button"){:.review_apps_button}

To access these pages you need to authenticate.
The credentials are as follows:
Username: `hifis`, Password: `HIFISReview!`.

As soon as someone reviews your blog post, all comments on your
_Merge Request_ appear as separate discussion threads which need
to be resolved by the reviewers before they can actually merge your 
changes into branch _master_.
This means that you first discuss and integrate the suggestions into
your post as well as secondly add and commit changes to your branch.

All opened discussion threads need to be resolved and closed manually
in GitLab before the final merge can take place by the reviewer.

Now, the reviewer finds a button "_Merge_" in that _Merge Request_ in
_GitLab_.
This tells _GitLab_ to integrate your changes into branch _master_.

By clicking that button a _GitLab CI (Continuous Integration) Pipeline_
<sup>[[14]](https://docs.gitlab.com/ce/ci/introduction/index.html "GitLab CI (Continuous Integration) Pipeline")</sup>
is started and makes checks whether _Jekyll_ can still build the
whole project.
In case all is fine, this is indicated by a success icon.

If the original issue has been referenced by `Closes #<issue-number>.`
the mentioned issue will be closed now.
Likewise, if the corresponding checkbox 
"_Delete source branch when merge request is accepted._" 
has been checked on creation of the _Merge Request_ in the 
_GitLab User-Interface_ the source branch will be deleted as well.
Finally, the _Merge Request_ will be closed and marked as _merged_.

**4. Your Post is Online**

Now your post is integrated and online. 
Well done!

That was fairly easy. 
Wasn't it? 
You have all the advantages of _Git_, _GitLab_ and _Jekyll_ 
at your disposal for quickly creating markdown-based blog posts 
of the highest quality.

If you are looking for a good _Git_ tutorial I would like to suggest
the one from the _Software Carpentry_ group
<sup>[[15]](http://swcarpentry.github.io/git-novice/ "Software Carpentry Git Lessons")</sup>.

_So what are the topics you would like to talk about today?_
Give it a try and let us know what your thoughts are which
you would like to share with us.

## References

* [[1]](https://gitlab.hzdr.de/hifis/software.hifis.net/blob/master/CONTRIBUTING.md "Contribution Guide") Contribution Guide
* [[2]](https://en.wikipedia.org/wiki/Git "Git (Wikipedia)") Git (Wikipedia)
* [[3]](https://en.wikipedia.org/wiki/GitLab "GitLab (Wikipedia)") GitLab (Wikipedia)
* [[4]](https://en.wikipedia.org/wiki/Jekyll_(software) "Jekyll (Wikipedia)") Jekyll (Wikipedia)
* [[5]](https://gitlab.hzdr.de "HZDR GitLab Instance") HZDR GitLab Instance
* [[6]](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html "Managing GitLab Issues") Managing GitLab Issues
* [[7]](https://docs.gitlab.com/ce/user/project/repository/web_editor.html#create-a-new-branch "Create a new Branch in GitLab") Create a new Branch in GitLab
* [[8]](hhttps://docs.gitlab.com/ce/user/project/web_ide/#open-the-web-ide "Open GitLab Web IDE") Open GitLab Web IDE
* [[9]](https://docs.gitlab.com/ce/user/markdown.html "GitLab Markdown") GitLab Markdown
* [[10]](https://docs.gitlab.com/ce/user/project/web_ide/#stage-and-commit-changes "Stage and Commit Changes in GitLab Web IDE") Stage and Commit Changes in GitLab Web IDE
* [[11]](https://chris.beams.io/posts/git-commit/ "Best Practice of Git Commit Messages") Best Practice of Git Commit Messages
* [[12]](https://docs.gitlab.com/ce/user/project/merge_requests/ "Merge Request") Merge Request
* [[13]](https://docs.gitlab.com/ce/user/project/merge_requests/creating_merge_requests.html "Creating a Merge Request in GitLab") Creating a Merge Request in GitLab 
* [[14]](https://docs.gitlab.com/ce/ci/introduction/index.html "GitLab CI (Continuous Integration) Pipeline") GitLab CI (Continuous Integration) Pipeline
* [[15]](http://swcarpentry.github.io/git-novice/ "Software Carpentry Git Lessons") Software Carpentry Git Lessons
