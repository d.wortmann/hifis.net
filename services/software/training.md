---
title: Education & Training
title_image: group-of-people-watching-on-laptop-1595385.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "We offer courses, materials or workshops to help you
  in getting started or in boosting your software engineering practice."
---

The purpose of the Education & Training component is to offer courses, materials, and workshops to help you in getting started, or to boost your engineering practices.
On this page, you can find upcoming workshops on a range of topics, as well as external resources to help you get started.

Can't find exactly what you're looking for?  Talk to us directly:
{% include contact_us/mini.html %}
{% include helpdesk/mini.html %}

# Upcoming Workshops

{% assign events = site.events | where: "type", "workshop" %}
{% include events/upcoming_events_list.html events=events limit=3 %}

# External Resources

These articles are written by third party organisations, and they aren't necessarily focussed on research software development, but they represent a set of industry best practices for writing software, sharing code, and building software communities.
We've brought these together here to try and answer some common questions that researchers often ask us.

### How Do I Share My Project?

* **Licensing** -
    Open source projects need an open source license - but how do I do that, and what license should I use?
    For a simple way to get started, try [Choose A License][choose-a-license], and for a more in-depth guide to creating open source projects, try [The Open Source Guide][open-source-guide/legal].
* **Citations** -
    It's important to prepare software projects for citation as part of sustainable research software development.
    Simple ways to both prepare for citation, and cite other researchers' software, are described at this [Research Software Citation][research-software/citation] guide.
* **Documentation** -
    Writing good documentation can be hard, but Write the Docs have put together a [Beginner's Guide to Writing Documentation][write-the-docs/beginners-guide], starting with a single README file.

If you're just starting out with this topic, and you have some code you want to make ready for publication/open source sharing, we run regular workshops where we can help you with all of these topics.
Click to [**see all our workshops**][internal/events] and find out more.

[choose-a-license]: <https://choosealicense.com/>
[open-source-guide/legal]: <https://opensource.guide/legal/>
[research-software/citation]: <https://research-software.org/citation>
[write-the-docs/beginners-guide]: <https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/>
[internal/events]: {% link events.md %}

### What Are Best Practices for Research Software?

* **Ethical Software** -
    Ethical research practices should also be visible in how we write research software.
    [The Turing Way][turing-way] is a guide to these best practices in data science, particularly reproducibility.
* **Software Layout** -
    Common patterns for software aim to make it as easy as possible for reviewers or new team members to understand how a project works.
    One pattern is using common project folder layouts.  The [Cookiecutter Data Science][cookiecutter-data-science] project provides a way to get started quickly on new data science projects in a manner that other people will be able to recognise and understand.
* **Committing** -
    Reading through commits in a shared project can be a valuable way of understanding changes that you or other people have made, and figuring out when a bug appeared.
    [How to Write a Git Commit Message][git-commit-message] explains how to describe the contents of a commit in a clean and readable way.
    Also, as a deep read, try [My favourite Git commit][my-favourite-commit].

[turing-way]: <https://the-turing-way.netlify.app/>
[cookiecutter-data-science]: <https://drivendata.github.io/cookiecutter-data-science/>
[git-commit-message]: <https://chris.beams.io/posts/git-commit/>
[my-favourite-commit]: <https://dhwthompson.com/2019/my-favourite-git-commit>

### Still Got Questions?

Let us know where you feel you need more support, what other questions you want answered, and what articles you'd find helpful:
{% include contact_us/mini.html %}
{% include helpdesk/mini.html %}
