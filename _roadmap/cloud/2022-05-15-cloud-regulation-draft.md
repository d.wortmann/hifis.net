---
date: 2022-05-15
title: Tasks in May 2022
service: cloud
---

## The draft of the Cloud Rulebook has been submitted to the Helmholtz Assembly of Members 
The Assembly of Members of the Helmholtz Association have received a draft for the definition of technical, administrative and data protection regulations to simplify the use of the Helmholtz Cloud and its Services, and to ensure that applicable conditions are met.
