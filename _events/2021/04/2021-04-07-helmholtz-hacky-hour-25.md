---
title: "Helmholtz Hacky Hour #25"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-04-07"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Pitfalls in data visualization</strong> Why people don't understand exponential growth and what color it has."
---
## Pitfalls in data visualization
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

There are as many ways to visualize data as there are to mess it up. Here we will talk about common pitfalls most of us are not aware of.

We are looking forward to seeing you!
