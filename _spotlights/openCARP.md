---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: openCARP

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-06-01

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: openCARP/openCARP_logo.png

# One or two sentences describing the software
excerpt: >
    openCARP is a multiscale cardiac electrophysiology
    simulator for in silico experiments ranging from single
    heart cells and cardiac tissue to organ models up to the body surface ECG.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: openCARP.jpg

# Title at the top, inside the title-content-container
title: openCARP

# Add at least one keyword
keywords:
    - modeling & simulation
    - computational cardiology
    - electrophysiology
    - in silico trials

# The Helmholtz research field
hgf_research_field: Health

# At least one responsible centre
hgf_centers:
    - Karlsruhe Institute of Technology (KIT)

# List of other contributing organisations (optional)
contributing_organisations:
    - name: Medical University of Graz
      link_as: https://ccl.medunigraz.at
    - name: Liryc Bordeaux
      link_as: https://www.ihu-liryc.fr/en/research/p/le-pole-modelisation-130750-23-07-2019/
    - name: Numericor
      link_as: https://www.numericor.at

# List of scientific communities
scientific_community:
    - Computational Cardiology

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: info@opencarp.org

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://www.openCARP.org
    - type: mailing-list
      link_as: https://www.openCARP.org/q2a/
    - type: twitter
      link_as: https://twitter.com/OpenCarp
    - type: gitlab
      link_as: https://git.opencarp.org/openCARP

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: Academic Public License

# Is the software pricey or free? (optional)
costs:

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Modeling & Simulation

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop
    - HPC

# List of programming languages (optional)
programming_languages:
    - C++
    - Python

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.1016/j.cmpb.2021.106223

# Funding of the software (optional)
funding:
    - shortname: DFG
      funding_text: German Research Foundation (Deutsche Forschungsgemeinschaft, DFG) -
        project 391128822
      link_as: https://gepris.dfg.de/gepris/projekt/391128822?language=en
    - shortname: EuroHPC
      funding_text: European High-Performance Computing Joint Undertaking EuroHPC (JU) under grant agreement No 955495. The JU receives support from the European Union’s Horizon 2020 research and innovation programme and France, Italy, Germany, Austria, Norway, Switzerland.
      link_as: http://microcard.eu/index-en.html

---

# openCARP - The open cardiac electrophysiology simulator

openCARP is an open cardiac electrophysiology simulator for in-silico experiments. Its source code is public and the software is freely available for academic purposes. openCARP is easy to use and offers single cell as well as multiscale simulations from ion channel to organ level. Additionally, openCARP includes a wide variety of functions for pre- and post-processing of data as well as visualization. The python-based CARPutils framework enables the user to develop and share simulation pipelines, i.e. automating in-silico experiments including all modeling/simulation steps.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/openCARP/openCARP_workflow.png" alt="openCARP workflow">
<span>Overview of typical steps in an advanced cardiac electrophysiology simulation study.</span>
</div>


openCARP offers a wide range of functionality which enables you to create your own in-silico experiments of cardiac electrophysiology. The openCARP ecosystem comprises several components that are visualized below and [briefly introduced here](https://www.openCARP.org/about/opencarp-ecosystem). [This video](https://www.openCARP.org/documentation/video-tutorials#a-tour-through-the-opencarp-org-community-platform) provides a tour through the [openCARP.org](https://opencarp.org/) community platform.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/openCARP/openCARP_ecosystem.png" alt="openCARP ecosystem">
<span>The openCARP ecosystem.</span>
</div>
