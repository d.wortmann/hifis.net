---
---
{% assign mathjax_path = site.directory.vendor | strip | append: "MathJax/" %}

// Acknowledgement, David Eppstein (Oct 17, 2019)
// https://11011110.github.io/blog/2019/10/17/mathjax-3-jekyll.html
document.addEventListener('DOMContentLoaded', function () {
    function stripcdata(x) {
        if (x.startsWith('% <![CDATA[') && x.endsWith('%]]>'))
            return x.substring(11, x.length - 4);
        return x;
    }
    document.querySelectorAll("script[type='math/tex']").forEach(function (el) {
        el.outerHTML = "\\(" + stripcdata(el.textContent) + "\\)";
    });
    document.querySelectorAll("script[type='math/tex; mode=display']").forEach(function (el) {
        el.outerHTML = "\\[" + stripcdata(el.textContent) + "\\]";
    });
    var script = document.createElement('script');
    script.src = "{{ mathjax_path | append: 'es5/tex-mml-chtml.js' | relative_url }}";
    script.defer = true;
    document.head.appendChild(script);
}, false);
