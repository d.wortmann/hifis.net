---
layout: spotlight

# Spotlight list attributes
name: IGMAS+
date_added: 2021-11-18
preview_image: igmas_logo.png
excerpt: Modern geophysical interpretation requires an interdisciplinary approach and software capable of handling multiple inhomogeneous data like seismic, FTG gravity, magnetic and magnetotelluric in complex geological environments.

# Title for individual page
title_image: igmas_timeline.png
title: IGMAS+ Interactive Gravity and Magnetic Application System
keywords:
    - Gravity
    - Magnetic
    - Modelling
hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
contributing_organisations:
scientific_community:
    - Structural Modelling
impact_on_community:
contact: igmas@gfz-potsdam.de
platforms:
    - type: webpage
      link_as: https://www.gfz-potsdam.de/igmas
    - type: telegram
      link_as: https://t.me/igmas
    - type: mailing-list
      link_as: mailto:igmas-users-subscribe@gfz-potsdam.de?subject=Subscribe&body=Please%20subscribe%20me%20to%20the%20IGMAS+%20Users%20mailing%20list.
license: Proprietary
costs: Free
software_type:
    - Modelling
application_type:
    - Desktop
programming_languages:
    - Java
doi: 10.1190/1.1442546
funding:
    - shortname: GFZ
      link_as: https://www.gfz-potsdam.de/en/home/  # Optional
---

# IGMAS+ in a nutshell

Modern geophysical interpretation requires an interdisciplinary approach and software capable of handling multiple inhomogeneous data like seismic, FTG gravity, magnetic and magnetotelluric in complex geological environments.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/igmas/western_south_american_margin.png" alt="3D flat model of the central Western South American margin, modelled gravity, and modelling constraings">
<span>3D flat model of the central Western South American margin, modelled gravity, and modelling constraings.</span>
</div>

IGMAS+ (Interactive Gravity and Magnetic Application System) is a geo-modelling software for three-dimensional joint inversion of potential fields and its derivatives under the condition of constraining data and independent information.

Three-dimensional gravity and magnetic modelling appreciably improves the results of distinct depth imaging projects. This regards especially to areas of strong lateral seismic velocity and density contrasts and corresponding imaging problems. Typical areas where grav/mag modelling has been successfully used are sub-salt and sub-basalt settings.

What makes IGMAS+ highly efficient and user-friendly is that it allows adjusting the geometries and physical properties of modelled subsurface bodies interactively, i.e. while the corresponding calculated and measured potential field components are visualized together with independent observations.

See our [main publication list](https://igmas.git-pages.gfz-potsdam.de/igmas-pages/about/#main), as well as [related publications](https://igmas.git-pages.gfz-potsdam.de/igmas-pages/about/#related) and [citing instructions](https://igmas.git-pages.gfz-potsdam.de/igmas-pages/citing).

## Functionality

A brief summary of recent functionality

* Fully featured graphical user interface
* Algorithm kernel
  * Polyhedron formula for triangulation, mass points and FFT for voxel cubes
  * Borehole gravity
  * Gravity field components Gx, Gy and Gz
  * Full tensor gravity gradient
  * Geoid
  * Invariants
  * Magnetic field components Bx, By and Bz
  * Remanent and induced rock magnetisation
  * Full tensor of magnetic gradients
* Model input
  * Import regular or irregular horizon data
  * Voxel cubes / Voxet
  * Simultaneous use of triangulated and gridded (voxel) data
* Geometry and parameter editor
  * Interactive modification of vertices and physical parameters with instant field updates
  * Optimisation (inversion) of physical parameters, including grouped voxel cell effects
  * Interactive geometry changes automatically affect the voxel cube
* Visualisation
  * Multiple undockable views (cross sections, maps)
  * Object browser including property editor for all model elements
* Multicore processing
* Multi language support
* Extensive User Manual
