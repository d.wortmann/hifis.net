---
title: "Recap: Workshop on RSE and FAIR at SE2022"
title_image: jason-goodman-Oalh2MojUuk-unsplash.jpg
date: 2022-06-08
authors:
  - klaffki
layout: blogpost
categories:
  - news
excerpt: >
  The workshop "Let's talk FAIR for research software" looked into current practises in RSE and the relation to the FAIR Principles.
---

## Research Software Engineering and the FAIR criteria
Martin Stoffers (DLR, HIFIS Consulting), Tobias Schlauch (DLR, HIFIS Education), Alexander Struck (Humboldt University) and René Caspart (KIT) organised the workshop "Let's talk FAIR for research software". 
The goal was to discuss practices in RSE in three parts: 
Based on the HIFIS workshop [Foundations of Research Software Publication](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials) they identified topics to include in a future version of said workshop, e.g. "Release Management / Publication" and "Life Cycle Management". 
The second part of the event connected the [FAIR Principles (Findablity, Accessibility, Interoperability, Reuse)](https://www.go-fair.org/fair-principles/) to high performance computing in general, 
while the last part deepened the aspect of Findable Research Software and how to overcome existing obstacles.

## Reaching out to relevant communities
The workshop covered topics highly relevant to HIFIS, but addressed a larger audience beyond Helmholtz: 
it took place at SE2022, the annual conference of the department Softwaretechnik of the German Gesellschaft für Informatik, this year in Berlin from 21th to 25th February. 
For more details and further reading see their just published [wrap-up of the event](https://de-rse.org/blog/2022/06/02/recap-workshop-lets-talk-fair-for-research-software-at-se2022.html).
