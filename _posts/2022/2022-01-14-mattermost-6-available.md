---
title: "Mattermost Major Release 6 available"
title_image: jamie-street-p2ifKHu3dXM-unsplash.jpg
date: 2022-01-14
authors:
  - huste
  - ziegner
  - hueser
layout: blogpost
categories:
  - news
excerpt: >
  Since January 14<sup>th</sup>, 2022, the Mattermost service in the Helmholtz Cloud
  appears in new splendor equipped with two new main features.
  Mattermost now comes with channels, boards and playbooks integrated
  into one single application.
---

## What's New

Since January 14<sup>th</sup>, 2022, the
[Mattermost service](https://mattermost.hzdr.de) in the
[Helmholtz Cloud](https://helmholtz.cloud) appears
in new splendor equipped with two new main features.
Mattermost now comes with
[channels](https://docs.mattermost.com/guides/channels.html),
[boards](https://docs.mattermost.com/guides/boards.html) and
[playbooks](https://docs.mattermost.com/guides/playbooks.html)
integrated into one single application.
The global navigation was redesigned providing Mattermost with a new look and feel.
All details and new features are described in the Mattermost release posts.

* <https://mattermost.com/blog/mattermost-v6-0-is-now-available/>
* <https://mattermost.com/blog/mattermost-v6-1-is-now-available/>

## Enhanced Documentation

We also took the opportunity to make it easier to get started
by extending the documentation.
The brand-new getting started guide helps you to quickly get in touch with the
service and the specifics of the installation.
We hope it helps to allow new colleagues and users to quickly get familiar
with Mattermost.
The most recent changes are now described in the Changelog.

* [Getting Started Guide](https://hifis.net/doc/software/mattermost/getting-started/)
* [Changelog](https://hifis.net/doc/software/mattermost/changelog/)

## Try it Out

Have you ever been annoyed by the unstructured views of the various topics
in channels?
Try out [Collapsed Reply Threads](https://hifis.net/doc/software/mattermost/getting-started/#collapsed-reply-threads)
by activating it in your user settings.

### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
