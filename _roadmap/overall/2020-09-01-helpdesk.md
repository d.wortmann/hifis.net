---
date: 2020-09-01
title: Tasks in Sept 2020
service: overall
---

## Start of HIFIS Helpdesk

A common contact point for all queries and support requests shall be introduced. This will be realized using a ticketing system, allowing to distribute tasks related to the different HIFIS clusters, especially Cloud Services and Software Services.
