---
title: "HIFIS website just got relaunched!"
title_image: jamie-street-_94HLr_QXo8-unsplash.jpg
date: 2022-04-06
authors:
  - klaffki
  - englaender
  - huste
  - jandt
  - spicker
layout: blogpost
categories:
  - news
excerpt: >
  New look and new contents — <a href="https://hifis.net">hifis.net</a> got a makeover to present the Helmholtz digital services.
---

### Have a look at the new HIFIS website that just got relaunched!

New look and new contents — [hifis.net]({% link index.md %}) got a makeover to present the Helmholtz digital services for science. Meet Sam the Scientist in our welcome video for a quick introduction what HIFIS is about!

The new navigation via the hamburger menu on the top right provides a faster overview about HIFIS: Get deeper insights to 1) our Helmholtz Services, 2) Learning & Training, 3) Use Cases, 4) HIFIS insights, 5) Media & Materials and 6) Our Support.

#### Applied RSE and Use Cases 
The new website focuses on the services available to all researchers within the Helmholtz community to facilitate their work. All of them are united in the [Helmholtz Cloud](https://helmholtz.cloud/services). If you are curious about what can be done via research software engineering (RSE), browse the [Software Spotlights]({% link spotlights.md %}) or the new category of [use cases]({% link usecases.md %}) which demonstrate the usage of HIFIS services. If you want your project or use case highlighted there, please get in touch with us.

#### Different interests, different access
Another way to enter the world of HIFIS are the buttons on the front page that are oriented along several user roles: Via different access points on the homepage, other stakeholders like [researchers]({% link hifisfor/scientists.md %}), [software engineers]({% link hifisfor/rse.md %}), (potential) [service providers]({% link services/cloud/provider.md %}) or [IT staff]({% link hifisfor/itsupport.md %}) will find information relevant to them more easily than before. Each page puts together introductory texts and links to the HIFIS offers that will hopefully suit your needs. If information is missing for your query, your welcome to [get in contact with us]({% link contact.md %}).

#### More to come in the future
The website team is going to add more content throughout the next weeks, e.g., more use cases. If you don't want to miss any of this, subscribe to the [RSS feed]({{ site.feed.collections.posts.path | relative_url }}) or the [newsletter](https://lists.desy.de/sympa/subscribe/hifis-announce), which will also get a makeover itself later this year.

### Helmholtz Digital Services for Science — Collaboration made easy.
