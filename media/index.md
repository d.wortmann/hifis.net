---
title: Media & Materials
title_image: default
layout: default
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
excerpt:
    HIFIS Media & Materials.
---

## General Publications

- [List of Publications on HIFIS]({{ "publications" | relative_url }})

## Media
- [Helmholtz Resonator Podcast, Episode 172]({% post_url 2021/10/2021-10-18-podcast %})
- HIFIS for Scientific Workflows
    - [Poster]({{ site.directory.images | relative_url }}/hifisfor/HIFIS_poster_claim_contact.svg)
    - [Video]({{ site.directory.videos | relative_url }}/video_full.mp4)
- Overview HIFIS: Digital Services for Helmholtz & Partners
    - [English Version]({{ 'media/HIFIS_overview_2022_03_en.pdf' | relative_url }})
    -  [German Version]({{ 'media/HIFIS_overview_2022_03_de.pdf' | relative_url }})

