---
title: Lecture series on scientific computing
layout: event
organizers:
  - dolling
  - belayneh
lecturers:
  - dolling
  - belayneh
type:   Lecture
start:
    date:   "2020-05-18"
    time:   "10:00"
end:
    date:   "2020-06-15"
    time:   "11:00"
location:
    campus: "Online Event"
excerpt:
    "This lecture series will teach researchers key topics in research software
    development including best practices"
---

## Goal

To teach researchers the basics of software development, and introduce them to 
existing tools and best practices for repoducable research software.


## Content

The lecture series is consists of the following topics: 
* Requirements for Code Publication (scheduled for 2020-05-18)
* Research software engineering (scheduled for 2020-05-25)
* Reusable Software (scheduled for 2020-06-08)
* Automation: making your results reproducible (scheduled for 2020-06-15)


## Requirements

No prior knowledge or experience is required.
