---
title: "Guidelines for Chatting"
title_image: volodymyr-hryshchenko-V5vqWC9gyEU-unsplash.jpg
date: 2020-05-15
authors:
  - Hammitzsch, Martin (GFZ) et al.
layout: blogpost
categories:
  - guidelines
excerpt:
redirect_from:
  - services/chatting/index_ger.html
  - services/chatting/index.html
  - guidelines/chatting/index_ger.html
  - guidelines/chatting/index.html
excerpt:
  Whenever you choose and set up a chat system you should as well implement a
  netiquette and best practices for you and your team members from the start.
lang: en
lang_ref: 2020-05-15-guidelines-for-chatting
---

## Scope
We’re talking about _Slack_, _Mattermost_, _Matrix_, _Rocket Chat_, _Jabber_,
_XMPP_, _ICQ_, and other services supporting direct communication with remote
team members either at the same time or asynchronously.

## Task
You're responsible for leading a group of people who work distributed in a team.
Besides email, chat can be a useful tool to coordinate your collaboration.

## Situation
You and your team cannot communicate directly with each other due to space or
time constraints.
Therefore you want to give your team a possibility to exchange information
easily and without problems.
In order for this exchange to be goal-oriented and fruitful,
a suitable tool and a set of rules must be discussed and set up.

## Challenge
Everyone is invited to participate actively while following rules that have
been negotiated and agreed upon by all team members thus enabling a smooth
communication and avoiding misunderstandings.

## Approach
Whenever you choose and set up a chat system you should as well implement a
netiquette and best practices for you and your team members from the start.

## Options
Below are a few suggestions that might make it into the set of rules for your
team to make everyone feel comfortable and more productive when using the chat.

- **Define the purpose**  
  Be clear what can be communicated via chat in your team.
  Chat does not completely replace email, but helps sufficiently keeping your
  email inbox organized and communication regarding specific issues or topics
  better inline - with a chat tool.
  For example, do not share bits and pieces by email, use the chat instead.

- **Propose chat rooms**  
  Make sure to create different chat rooms for different topics in case your
  team is involved in various activities and interests in different constellations.
  However, avoid cluttered communication, e.g. originating from too many chat rooms.

- **Offer space for socializing**  
  Besides topical chat rooms, offer a „town hall“ or “campfire” for off-topic
  conversations.
  Keep off-topic conversation appropriate for your professional setting and
  limit the amount.
  Also use it to chat with only three or five of the team, e.g. to find a time
  to meet and to avoid the creation of additional chat rooms in any combination
  possible.
  Other team members will appreciate it to see that their peers are alive even
  if the chat isn't something they are directly involved in.
  You even may use it to send out a “ping” each morning to let others know
  that they are not alone.

- **Foster direct chat communication**  
  Beyond chat rooms with three or more team members you might foster direct
  communication for asking and answering quick questions, sharing immediate
  information, getting in touch to arrange voice or video calls at a later
  time, or clarifying and discussing facts in a lively back and forth
  communication without phoning each other.

- **Define response times and urgency**  
  Clarify what the acceptable and expected response time is.
  A direct chat might be urgent in some cases whereas a chat room most likely
  is not.
  So make clear statements and agreements on the response time to be expected -
  if there are any to be expected.

- **Set a status message**  
  Include details in your chat profile you want others to see thus giving them
  more information about what you are up to, where you are and whether you
  are available.
  It helps others to identify if you cannot reply quickly, e.g. when attending
  a meeting, or if you are not available in person, e.g. when being in
  home office.
  Also, be aware of the status messages of your peers.

- **Respect the working methods of your peers**  
  end chats only to relevant people or to the relevant room.
  Use chats only for short conversations so you do not take too much time away
  from the people on the other end. Don’t be distracting -
  if a status message indicates that your peer is away or busy you may
  postpone the communication or send an email instead.
  Also, take care to get not distracted by chats from your work you currently
  focus at - find a routine to come around from time to time.

- **Stay structured and organized**  
  Stay on topic and keep the conversation short.
  Send consolidated messages, avoid sending several messages in quick succession.
  Reply only to messages directed at you or reply if you are able to contribute
  meaningfully.
  Use threads to organize discussions by replying to a specific message,
  if possible.

- **Be friendly and inviting**  
  Start with a short greeting or seek permission.
  Reply quickly or communicate that you cannot, e.g. by using a status message.
  End conversations with a short closing and a thank you, when appropriate.

- **Be sensitive**  
  Be particularly cautious about joking, humorous comments, and sharing
  personal information - things may come across differently than expected
  when not talking in person.
  Also, never write anything you wouldn’t say aloud.

- **Be aware of the limitations of written communication**  
  Never send bad news via chat or negative feedback.
  Do not use chat for emotionally loaded topics.
  A face-to-face communication or a talk in person includes nonverbal behaviors -
  e.g. the gestures and eye contact you make, your posture, and your tone
  and pitch of voice.
  These wordless signals cannot be given and received in a chat and limit the
  communication.
  Emojis were introduced to help in this regard for private chats but should
  be used wisely in professional chat communications.

- **Be mindful of spelling, formatting, and other mechanics**  
  Check your wording and spelling and possible mistakes produced by autocorrection.
  Be appropriate with emojis, all caps and exclamation marks.
  Be careful with abbreviations and communicate clearly - use enough words to make
  your message understandable to all chat participants and avoid slang.

- **Use encryption for sensitive and personal information**  
  Keep an eye on data privacy and protection.
  Never share sensitive or confidential information in unsecured chats or
  infrastructure of which you are not sure it is operated within your legal
  constraints, e.g. outside the EU.
  If there is a possibility that you share sensitive or personal information
  via chat - the probability is high if you do more than share cat videos -
  make sure that you use end-to-end encryption for your chat,
  or point-to-point, if it is a requirement that your infrastructure
  provider can listen in.
  Many clients have plugins for encryption with PGP, for direct communication
  between two users with OTR, or for group chat with OMEMO.

- **Stick with other communication channels**  
  If communication started through a different channel than chat, stick with it!
  When changing communication channels in-between then a communication breaks
  apart and others will be lost.
  For example, don't change meeting times or venues in a chat, use the medium
  originally used to set up the meeting.

_These recommendations have been created in collaboration with
Maryna Bondarava (HMGU), Patrick Preuster (FZJ), Carsten Schirnick (GEOMAR) and
Mario Strefler (KIT)._

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
