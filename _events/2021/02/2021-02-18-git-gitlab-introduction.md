---
title: Introduction to Git and GitLab
layout: event
organizers:
  - schlauch
lecturers:
  - schlauch
  - "Carina Haupt (DLR)"
  - "Michael Meinel (DLR)"
type:   workshop
start:
    date:   "2021-02-18"
    time:   "09:00"
end:
    date:   "2021-02-19"
    time:   "13:00"
registration_link: https://events.hifis.net/event/45/registrations/40/
location:
    campus: "Online Event"
fully_booked_out: true
registration_period:
    from:   "2021-02-01"
    to:     "2021-02-12"
excerpt:
    "This workshop provides an introduction into Git in combination with the collaboration platform GitLab."
---

## Goal

The workshop provides a solid introduction into the practical usage of the version control system Git in combination with the collaboration platform GitLab.

## Content

This workshop will cover the the following topics:
- Introduction to version control
- Git setup
- Basic local Git workflow
- Git branches and handling of conflicts
- Collaboration with others

Please see the [lesson materials](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material) and the [registration page](https://events.hifis.net/event/45/) for further details.

## Requirements

- No previous knowledge in the covered topics will be required. 
- Participants require the Git command line client, a modern web browser, and a text editor.
